package test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlRootElement;

@Path("/helo")
@XmlRootElement
public class Hello {
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public String sayHello_XML()
	{
		String resource = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"+
"<aliens><alien><id>1</id><name>jaga</name> <points>20</points></alien><alien><id>2</id> <name>Navin</name> <points>70</points></alien></aliens>";
		return resource;
	}
/*	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String sayHello_Json()
	{
		String resource = "Json";
		return resource;
	}*/
	
	/*@GET
	@Produces(MediaType.TEXT_HTML)
	public String sayHello()
	{
		String resource = "HTML";
		return resource;
	}*/
}
